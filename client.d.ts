/// <reference types="@xyh19/rollup-plugin-assets/global" />

declare module 'process' {
  import { IHotModuleApi } from '@xyh19/hot-module-cjs'

  global {
    interface NodeModule {
      hot: IHotModuleApi
    }

    const __DEV__: boolean
    const __PROD__: boolean
    const __RENDERER__: string
    namespace NodeJS {
      interface ProcessEnv {
        DEV_PORT: string
        DEV_URL: string
      }
    }
  }
}

declare module 'electron-squirrel-startup' {
  const result: boolean
  export = result
}
