import { defaultArchFromString } from 'builder-util'
import { Arch, build, Configuration, FileSet } from 'electron-builder'
import { normalizePath } from 'vite'
import { UserOptions } from './options'
import { packageJson, toArray } from './utils'

export const buildElectron = async (options: UserOptions, outputDir: string) => {
  const archOpts = process.argv
    .filter((v) => v.startsWith('--'))
    .map((v) => {
      try {
        return defaultArchFromString(v.substring(2))
      } catch {}
    })
    .filter((v) => !!v) ?? [defaultArchFromString()]

  let packageBuild: Configuration | undefined = packageJson?.build

  const directories = {
    output: packageBuild?.directories?.output || options.build?.directories?.output || 'build',
    app: packageBuild?.directories?.app || options.build?.directories?.app,
    buildResources:
      packageBuild?.directories?.buildResources || options.build?.directories?.buildResources,
  }

  await build({
    x64: archOpts.includes(Arch.x64),
    ia32: archOpts.includes(Arch.ia32),
    armv7l: archOpts.includes(Arch.armv7l),
    arm64: archOpts.includes(Arch.arm64),
    universal: archOpts.includes(Arch.universal),
    dir: process.argv.includes('--dir'),
    config: {
      asar: false,
      win: { target: 'nsis' },
      ...packageBuild,
      ...options.build,
      files: [
        {
          from: normalizePath(outputDir),
          to: '.',
        },
        'package.json',
        ...toArray<string | FileSet>(packageBuild?.files).filter(Boolean),
        ...toArray<string | FileSet>(options.build?.files).filter(Boolean),
      ],
      directories,
    },
  })
}
