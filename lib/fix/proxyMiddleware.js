// 修复http-proxy-middleware模块热替换导致createProxyMiddleware函数丢失的问题
import { createProxyMiddleware } from 'http-proxy-middleware'
global._createProxyMiddleware = createProxyMiddleware ?? global._createProxyMiddleware
const _createProxyMiddleware = global._createProxyMiddleware
export { _createProxyMiddleware as createProxyMiddleware }
